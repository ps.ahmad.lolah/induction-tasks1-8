package com.progressoft.JIP8.Lab5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class Password {

    @Test
    public void testPasswordLength() {
        for (int i = 0; i < 100000; i++) {
            Assertions.assertEquals(8, PasswordGenerator.generatePassword().length());
            Assertions.assertEquals(8, PasswordGenerator.generatePassword().length());
        }

    }

    @Test
    public void testPasswordRandomise() {
        ArrayList<StringBuilder> Passwords = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            StringBuilder password = PasswordGenerator.generatePassword();
            Assertions.assertFalse(Passwords.contains(password));
            Passwords.add(password);
        }
    }

    @Test
    public void testPasswordChars() {
        for (int i = 0; i < 10; i++) {
            TestPasswordChars.check(PasswordGenerator.generatePassword());
            Assertions.assertEquals(2, TestPasswordChars.symbols_counter);
            Assertions.assertEquals(2, TestPasswordChars.letter_counter);
            Assertions.assertEquals(4, TestPasswordChars.digits_counter);
        }
    }


    private static class TestPasswordChars {
        public static int symbols_counter;
        public static int letter_counter;
        public static int digits_counter;

        public static void check(StringBuilder password) {
            symbols_counter=0;
            letter_counter=0;
            digits_counter=0;

            for (int i = 0; i < password.length(); i++) {
                char charAt = password.charAt(i);
                if (Character.isDigit(charAt)) {
                    digits_counter++;
                } else {
                    if (Character.isLetter(charAt) && Character.isUpperCase(charAt))
                        letter_counter++;
                    else {
                        if (charAt == '$' || charAt == '_' || charAt == '#' || charAt == '%')
                            symbols_counter++;
                        else Assertions.fail("invalid Password");
                    }
                }
            }
        }

    }
}
