package com.progressoft.JIP8.Lab5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PasswordGenerator {
    private static List<Character> password;
    private static final int size = 8;

    public static StringBuilder generatePassword() {
        final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        final String symbols = "_$#%";
        final String numbers = "0123456789";
        password = new ArrayList<>(size);

        addRandomChars(numbers, 4);
        addRandomChars(letters, 2);
        addRandomChars(symbols, 2);

        return shufflePassword();
    }

    private static void addRandomChars(String addition, int repeat) {
        Random random = new Random();

        for (int i = 0; i < repeat; i++)
            password.add(addition.charAt(random.nextInt(addition.length() - 1)));
    }

    private static StringBuilder shufflePassword() {
        StringBuilder shuffled = new StringBuilder(size);
        for (int i = 0; i < size / 2; i++) {
            shuffled.append(password.get(i));
            shuffled.append(password.get(i + size / 2));
        }
        if (size % 2 == 1) {
            shuffled.append(password.get(size - 1));
        }


//        shuffle(password);
//        StringBuilder shuffled = new StringBuilder(8);
//        for (char el : password) {
//            shuffled.append(el);
//        }

//        System.out.println(shuffled);
        return shuffled;
    }
}
