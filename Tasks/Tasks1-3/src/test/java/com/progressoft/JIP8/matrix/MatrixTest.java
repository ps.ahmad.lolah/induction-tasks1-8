package com.progressoft.JIP8.matrix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import java.util.Random;

public class MatrixTest {

    @Test
    public void givenInconsistentMatrix_whenCreateMatrix_thenThrowIllegalArgumentException() {
        int[][] matrix = {{1}, {2, 3}};

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Matrix(matrix);
        });

        Assertions.assertEquals(exception.getMessage(),"inconsistent Matrix");
    }

    @RepeatedTest(10)
    public void givenNullRow_whenCreateMatrix_thenThrowIllegalArgumentException() {
        int[][] matrix = {{0, 1}, {2, 3}, {4, 5}};
        int defectedRow = new Random().nextInt(matrix.length);
        matrix[defectedRow] = null;

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Matrix(matrix);
        });
        Assertions.assertEquals(exception.getMessage(),"row "+defectedRow+" is Null");
    }


    @Test
    public void given2DArray_whenCreateMatrix_thenCreateObject()
    {
        Matrix matrix = new Matrix(new int[][]{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}});
        String MatrixClass = String.valueOf(matrix.getClass());
        Assertions.assertTrue(MatrixClass.equals("class com.progressoft.JIP8.matrix.Matrix"));
    }

}
