package com.progressoft.JIP8.matrix;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixUtilityTest {
    @Test
    public void givenMatrix_whenSum_thenReturnResultOfSumAsMatrix() {
        Matrix matrix = new Matrix(new int[][]{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}});

        int[][] sum = {{0, 0, 0}, {2, 2, 2}, {4, 4, 4}};

        Assertions.assertEquals(MatrixUtility.sum(matrix, matrix), new Matrix(sum));
    }

    @Test
    public void givenInvalidMatrixForSumOperation_whenSum_thenIllegalArgumentException() {
        Matrix matrix1 = new Matrix(new int[3][3]);
        Matrix matrix2 = new Matrix(new int[4][5]);
        ArithmeticException exception = Assertions.assertThrows(ArithmeticException.class, () -> {
            MatrixUtility.sum(matrix1, matrix2);
        });

        Assertions.assertEquals(exception.getMessage(), "invalid Operation");

    }

    @Test
    public void givenMatrix_whenScale_thenReturnScaledMatrix() {
        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);
        int scale = 10;

        int[][] result = {{0, 0, 0}, {10, 10, 10}, {20, 20, 20}};
        Assertions.assertEquals(MatrixUtility.scale(matrix, scale), new Matrix(result));
    }

    @Test
    public void givenMatrix_whenTranspose_thenReturnTransposedMatrix() {
        int[][] result = {{0, 1, 2}, {0, 1, 2}, {0, 1, 2}};
        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);

        Matrix transposed = MatrixUtility.transpose(matrix);
        Assertions.assertNotNull(transposed);
        Assertions.assertEquals(transposed, new Matrix(result));
    }

    @Test
    public void givenMatrix_whenMultiply_thenReturnResultOfMultiplicationAsMatrix() {

        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);

        Matrix expected = new Matrix(new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}});

        Matrix result = MatrixUtility.multiply(expected, matrix);

        Assertions.assertEquals(result, expected);

    }

    @Test
    public void givingIncompatibleMatrixForMultiply_whileMultiply_thenThrowArithmeticException() {
        Matrix matrix = new Matrix(new int[3][3]);
        Matrix matrix1 = new Matrix(new int[4][5]);

        ArithmeticException exception =
                Assertions.assertThrows(ArithmeticException.class, () -> {
                    MatrixUtility.multiply(matrix1, matrix);
                });
        Assertions.assertEquals(exception.getMessage(), "incompatible for multiplication");

    }

    @Test
    public void givenInvalidMatrixForMultiplyOperation_whenMultiply_givenIllegalArgumentException() {
        Matrix matrix = new Matrix(new int[4][5]);
        Matrix matrix1 = new Matrix(new int[6][5]);
        Assertions.assertThrows(ArithmeticException.class, () -> {
            MatrixUtility.multiply(matrix, matrix1);
        });

    }


    @Test
    public void givenInvalidRowOrColumn_whenSubMatrix_givingThrowIllegalArgumentException() {

        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class, () ->
                        MatrixUtility.subMatrix(matrix, -1, 0));
        Assertions.assertEquals(exception.getMessage(), "Invalid row or column");

        exception =
                Assertions.assertThrows(IllegalArgumentException.class, () ->
                        MatrixUtility.subMatrix(matrix, 0, -1));
        Assertions.assertEquals(exception.getMessage(), "Invalid row or column");

        exception =
                Assertions.assertThrows(IllegalArgumentException.class, () ->
                        MatrixUtility.subMatrix(matrix, 0, 4));
        Assertions.assertEquals(exception.getMessage(), "Invalid row or column");

    }

    @Test
    public void givingMatrix_whileSubMatrix_thenReturnSubMatrix() {
        int[][] result = {{1, 1}, {2, 2}};
        Matrix matrix = new Matrix(new int[][]{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}});

        Assertions.assertEquals(new Matrix(result), MatrixUtility.subMatrix(matrix, 0, 0));
    }

    @Test
    public void givingSquareMatrix_whileDeterminant_thenReturnDeterminant() {
        Matrix matrix = new Matrix(new int[][]{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}});

        Assertions.assertEquals(MatrixUtility.determinant(matrix), 0);

    }

    @Test
    public void givingUnsquareMatrix_whileDeterminant_thenThrowArithmeticException() {
        Matrix matrix = new Matrix(new int[4][5]);
        ArithmeticException exception =
                Assertions.assertThrows(ArithmeticException.class, () -> MatrixUtility.determinant(matrix));
        Assertions.assertEquals(exception.getMessage(), "unSquare Matrix");

    }

    @Test
    public void givingSquareMatrix_whileDiagonal_thenReturnDiagonalMatrix() {
        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);
        int[][] result = {{0, 0, 0}, {0, 1, 0}, {0, 0, 2}};
        Assertions.assertEquals(MatrixUtility.diagonalMatrix(matrix), new Matrix(result));

    }

    @Test
    public void givingUnsquareMatrix_whileDiagonal_then() {
        Matrix matrix = new Matrix(new int[4][5]);

        ArithmeticException exception =
                Assertions.assertThrows(ArithmeticException.class, () -> MatrixUtility.diagonalMatrix(matrix));
        Assertions.assertEquals(exception.getMessage(), "unSquare Matrix");
    }

    @Test
    public void givingSquareMatrix_whileToUpper_thenReturnUpperMatrix() {
        int[][] arr = {{0, 0, 0}, {1, 1, 1}, {2, 2, 2}};
        Matrix matrix = new Matrix(arr);
        int[][] result = {{0, 0, 0}, {0, 1, 1}, {0, 0, 2}};
        Assertions.assertEquals(MatrixUtility.toUpper(matrix), new Matrix(result));
    }

    @Test
    public void givingUnsquareMatrix_whileToUpper_givingThrowArithmeticException() {
        Matrix matrix = new Matrix(new int[4][5]);

        ArithmeticException exception =
                Assertions.assertThrows(ArithmeticException.class, () -> MatrixUtility.toUpper(matrix));
        Assertions.assertEquals(exception.getMessage(), "unSquare Matrix");

    }

    @Test
    public void givingSquareMatrix_whileToLower_thenReturnLowerMatrix() {
        Matrix matrix = new Matrix(new int[][]{{0, 0, 0}, {1, 1, 1}, {2, 2, 2}});
        Matrix matrix2 = new Matrix(new int[4][5]);

        int[][] result = {{0, 0, 0}, {1, 1, 0}, {2, 2, 2}};
        Assertions.assertEquals(MatrixUtility.toLower(matrix), new Matrix(result));

    }

    @Test
    public void givingUnsquareMatrix_whileToLower_givingThrowArithmeticException() {
        Matrix matrix = new Matrix(new int[4][5]);

        ArithmeticException exception =
                Assertions.assertThrows(ArithmeticException.class, () -> MatrixUtility.toLower(matrix));
        Assertions.assertEquals(exception.getMessage(), "unSquare Matrix");

    }
}
