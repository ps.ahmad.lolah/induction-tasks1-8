package com.progressoft.JIP8.graphs;

public class BinaryTree {

    private Node root;

    public BinaryTree() {
        root = null;
    }

    public int depth(int data) {
        Node pointer = root;
        int depth = 0;

        while (pointer != null) {
            if (pointer.data == data)
                return depth;
            if (data > pointer.data)
                pointer = pointer.right;
            else
                pointer = pointer.left;
            depth++;
        }

        throw new IllegalArgumentException("The Element is not exist");
    }

    public int treeDepth() {
        if (isEmpty())
            throw new IllegalStateException("Empty Tree");

        return treeDepth(root);
    }

    private boolean isEmpty() {
        return root == null;
    }

    private int treeDepth(Node node) {

        System.out.println(node.data);
        if (isLeaf(node)) {
            return 0;
        }

        int rightdepth = 0;
        int leftdepth = 0;

        if (node.right != null)
            rightdepth = treeDepth(node.right);
        if (node.left != null)
            leftdepth = treeDepth(node.left);

        return 1 + (Math.max(rightdepth, leftdepth));
    }

    public boolean accept(int data) {
        return accept(root, data);
    }

    private boolean accept(Node node, int data) {

        if (node == null)
            return true;

        if (data == node.data)
            return false;

        if (data < node.data)
            return accept(node.left, data);
        else
            return accept(node.right, data);
    }

    public void insert(int data) {
        root = insert(root, data);
    }

    private Node insert(Node node, int data) {
        if (node == null) {
            return new Node(data);
        }

        if (data == node.data)
            return node;

        if (data < node.data)
            node.left = insert(node.left, data);
        else
            node.right = insert(node.right, data);

        return node;
    }

    void remove(int key) {
        if (isEmpty())
            throw new IllegalStateException("Empty Tree");

        root = remove(root, key);
    }

    Node remove(Node node, int key) {
        if (node == null)
            throw new IllegalArgumentException("The Element is not exist");

        if (key < node.data)
            node.left = remove(node.left, key);
        else if (key > node.data)
            node.right = remove(node.right, key);

        else {
            if (node.left == null)
                return node.right;
            else if (node.right == null)
                return node.left;

            node.data = minValue(node.right).data;

            node.right = remove(node.right, node.data);
        }

        return node;
    }

    private Node minValue(Node node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

    private boolean isLeaf(Node node) {
        return node.right == null && node.left == null;
    }

    private class Node {
        int data;
        Node left, right;

        public Node(int data) {
            this.data = data;
            left = right = null;
        }
    }


}
