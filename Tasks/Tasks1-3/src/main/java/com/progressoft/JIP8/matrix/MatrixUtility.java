package com.progressoft.JIP8.matrix;

public class MatrixUtility {

    public static Matrix multiply(Matrix matrix, Matrix matrix1) {
        if (matrix == null)
            throw new IllegalArgumentException("null matrix");


        if (matrix1.getColumns() != matrix.getRows()) {
            throw new ArithmeticException("incompatible for multiplication");
        }

        int columns = matrix.getColumns();
        int[][] result = new int[matrix1.getRows()][columns];

        for (int row = 0; row < matrix1.getRows(); row++) {
            for (int col = 0; col < columns; col++) {
                // TODO recheck the condition "Done"
                for (int readIndex = 0; readIndex < matrix1.getColumns(); readIndex++) {
                    result[row][col] += matrix1.getMatrix()[row][readIndex] * matrix.getValue(readIndex, col);
                }
            }
        }
        return new Matrix(result);
    }

    public static Matrix sum(Matrix matrix, Matrix matrix1) {
        if (matrix == null)
            throw new IllegalArgumentException("null matrix");

        if (matrix.getColumns() != matrix1.getColumns() || matrix.getRows() != matrix1.getRows()) {
            throw new ArithmeticException("invalid Operation");
        }

        int[][] sum = new int[matrix.getRows()][matrix.getColumns()];
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                sum[i][j] = matrix.getMatrix()[i][j] + matrix1.getMatrix()[i][j];
            }
        }
        return new Matrix(sum);

    }

    public static int determinant(Matrix matrix) {
        if (!isSquare(matrix)) {
            throw new ArithmeticException("unSquare Matrix");
        }

        int result = determinant(matrix.getMatrix(), matrix.getRows(), matrix.getColumns());
        return result;
    }

    public static boolean isSquare(Matrix matrix) {
        return matrix.getRows() == matrix.getColumns();
    }

    private static int determinant(int[][] matrix, int rows, int columns) {
        if (columns == 2 && rows == 2) {
            return calculateDeterminant(matrix);
        }

        int result = 0;
        int sign = 1;
        for (int i = 0; i < rows; i++) {
            int[][] subMatrix = subMatrix(matrix, i, 0);
            result += sign * matrix[0][i] * determinant(subMatrix, rows - 1, columns - 1);
            sign *= -1;
        }
        return result;
    }

    private static int[][] subMatrix(int[][] matrix, int rowToRemove, int colToRemove) {

        int[][] result = new int[matrix.length - 1][matrix[0].length - 1];
        int destRow;
        for (int row = 0; row < matrix.length; row++) {
            if (row == rowToRemove) {
                continue;
            }
            if (row > rowToRemove)
                destRow = row - 1;
            else
                destRow = row;

            for (int col = 0; col < matrix.length; col++) {
                if (col == colToRemove)
                    continue;

                if (col > colToRemove) {
                    result[destRow][col - 1] = matrix[row][col];
                } else {
                    result[destRow][col] = matrix[row][col];
                }
            }
        }
        return result;

    }


    private static int calculateDeterminant(int[][] matrix) {

        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }


    // TODO return a Matrix "Done"
    public static Matrix scale(Matrix matrix, Integer value) {
        if (value == null) {
            throw new IllegalArgumentException("null Value");
        }

        Matrix result = new Matrix(new int[matrix.getRows()][matrix.getColumns()]);
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                result.setMatrix(value * matrix.getMatrix()[i][j], i, j);
            }
        }
        return result;

    }


    public static Matrix transpose(Matrix matrix) {
        Matrix result = new Matrix(new int[matrix.getColumns()][matrix.getRows()]);
        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                result.setMatrix(matrix.getMatrix()[i][j], j, i);
            }
        }
        return result;
    }

    public static Matrix subMatrix(Matrix matrix, int row, int column) {
        if (!matrix.InRange(row, column))
            throw new IllegalArgumentException("Invalid row or column");

        return new Matrix(subMatrix(matrix.getMatrix(), row, column));
    }


    public static Matrix diagonalMatrix(Matrix matrix) {
        if (!isSquare(matrix))
            throw new ArithmeticException("unSquare Matrix");

        Matrix result = new Matrix(new int[matrix.getRows()][matrix.getColumns()]);

        for (int i = 0; i < matrix.getColumns(); i++) {
            result.setMatrix(matrix.getMatrix()[i][i], i, i);
        }
        return result;
    }

    public static Matrix toLower(Matrix matrix) {
        if (!isSquare(matrix))
            throw new ArithmeticException("unSquare Matrix");


        Matrix result = new Matrix(new int[matrix.getRows()][matrix.getColumns()]);

        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j <= i; j++)
                // TODO revisit read sequence "Done"
                result.setMatrix(matrix.getMatrix()[i][j], i, j);
        }
        return result;
    }

    public static Matrix toUpper(Matrix matrix) {
        if (!isSquare(matrix))
            throw new ArithmeticException("unSquare Matrix");

        Matrix result = new Matrix(new int[matrix.getRows()][matrix.getColumns()]);

        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = i; j < matrix.getColumns(); j++)
                result.setMatrix(matrix.getMatrix()[i][j], i, j);
        }
        return result;
    }


}
