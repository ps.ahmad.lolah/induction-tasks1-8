package com.progressoft.JIP8.matrix;

import java.util.Arrays;
import java.util.Objects;

public class Matrix {
    private final int[][] matrix;
    private final int rows;
    private final int columns;

    public Matrix(int[][] matrix) {
        if (matrix == null)
            throw new IllegalArgumentException("null Matrix");


        // TODO verify if it is a matrix or not "Done"
        isValid(matrix);
        // TODO copy "Done"

        this.rows = matrix.length;
        this.columns = matrix[0].length;
        this.matrix = matrix.clone();

    }

    private void isValid(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            if (matrix[row] == null)
                throw new IllegalArgumentException("row " + row + " is Null");
            if (matrix[row].length != matrix[0].length)
                throw new IllegalArgumentException("inconsistent Matrix");
        }
    }


    public void setMatrix(int value, int row, int column) {
        // TODO you need to check if the row and column within ranges "Done"
        if (!InRange(row, column))
            throw new IllegalArgumentException("Invalid row or column");

        this.matrix[row][column] = value;
    }

    protected boolean InRange(int row, int column) {
        return row < this.rows && row >= 0 && column < this.columns && column >= 0;

    }

    public int getValue(int row, int column) {

        if (!InRange(row, column))
            throw new IllegalArgumentException("Invalid row or column");

        return this.matrix[row][column];
    }

    @Override
    public boolean equals(Object o) {
        if (this.getClass() != o.getClass())
            return false;

        if (this == o) return true;

        Matrix anotherMatrix = (Matrix) o;

        if (this.rows != anotherMatrix.rows || this.columns != anotherMatrix.columns)
            return false;

        for (int i = 0; i < anotherMatrix.rows; i++) {
            int[] row = this.matrix[i];
            int[] anotherRow = anotherMatrix.matrix[i];

            if (!Arrays.equals(row, anotherRow)) {
                System.out.println("false");
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(rows, columns);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int[][] getMatrix() {
        // TODO return a copy from it "Done"
        return matrix.clone();
    }

}
