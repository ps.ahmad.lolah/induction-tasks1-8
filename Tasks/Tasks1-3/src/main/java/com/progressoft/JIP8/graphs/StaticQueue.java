package com.progressoft.JIP8.graphs;

public class StaticQueue extends Queue {
    private final int size;

    public StaticQueue(int size) {
        super();
        this.size = size;
    }

    public void enqueue(int value) {
        if (isFull()) {
            throw new IllegalStateException("The Queue is full");
        }
        super.enqueue(value);
    }

    private boolean isFull() {
        return n == size;
    }

}
