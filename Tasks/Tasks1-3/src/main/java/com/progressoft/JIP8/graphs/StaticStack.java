package com.progressoft.JIP8.graphs;

public class StaticStack extends Stack {
    private final int size;

    public StaticStack(int size) {
        super();
        this.size = size;
    }

    public void push(int value) {

        if (isFull()) {
            throw new IllegalStateException("The Stack is full");
        }
        super.push(value);
    }

    public boolean isFull() {
        return n == size;
    }


}
