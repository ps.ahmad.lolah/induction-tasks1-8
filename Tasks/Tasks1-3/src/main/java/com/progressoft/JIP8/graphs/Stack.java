package com.progressoft.JIP8.graphs;

public class Stack {

    protected Node head;
    protected int n;

    public Stack() {
        this.head = null;
        this.n = 0;
    }


    public void push(int value) {
        if (isEmpty())
            head = new Node(value);
        else {
            Node newNode = new Node(value);
            newNode.next = head;
            head = newNode;
        }
        n++;
    }

    public int pop() {
        if (isEmpty())
            throw new IllegalStateException("Empty Stack");

        Node temp = head;
        head = head.next;
        n--;
        return temp.value;
    }

    public int peek() {
        if (isEmpty())
            throw new IllegalStateException("Empty Stack");
        return head.value;
    }

    public boolean isEmpty() {
        return n == 0;
    }

}
