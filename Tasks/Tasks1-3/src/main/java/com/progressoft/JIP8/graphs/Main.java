package com.progressoft.JIP8.graphs;

public class Main {

    public static void main(String[] args) {
//    StaticStack stack =  new StaticStack(4);
//
//    stack.push(0);
//    stack.push(55);
//    stack.push(555);
//    stack.push(5555);
//
//
//    stack.pop();
//    stack.pop();
//    stack.pop();
//
//    System.out.println(stack.peek());

//    StaticQueue queue = new StaticQueue(5);
//
//    queue.enqueue(5);
//    queue.enqueue(10);
//    queue.enqueue(20);
//
//    queue.dequeue();
//    queue.dequeue();
//
//    System.out.println(queue.peek());
//

        BinaryTree tree = new BinaryTree();
        tree.insert(10);
        tree.insert(20);
        tree.insert(30);
        tree.insert(60);
        tree.insert(50);
        tree.insert(11);
        tree.insert(12);
        tree.insert(13);
        tree.insert(14);
        tree.insert(17);
        tree.remove(11);
        tree.insert(11);
        // tree.remove(17);


        System.out.println(tree.treeDepth());
    }
}
