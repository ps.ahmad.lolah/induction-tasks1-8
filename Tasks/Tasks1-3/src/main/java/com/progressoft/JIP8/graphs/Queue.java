package com.progressoft.JIP8.graphs;

public class Queue {
    protected Node head;
    protected Node tail;
    protected int n;

    public Queue() {
        n = 0;
        this.head = null;
        this.tail = null;
    }

    public void enqueue(int value) {
        if (isEmpty())
            tail = head = new Node(value);
        else {
            Node temp = new Node(value);
            tail.next = temp;
            tail = temp;
        }
        n++;
    }

    public int dequeue() {
        if (isEmpty())
            throw new IllegalStateException("Empty Queue");

        Node temp = head;
        head = head.next;
        n--;

        return temp.value;
    }

    public int peek() {
        if (isEmpty())
            throw new IllegalStateException("Empty Queue");
        return head.value;
    }


    public boolean isEmpty() {
        return n == 0;
    }



}
