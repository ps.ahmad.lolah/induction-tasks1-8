package com.progressoft.JIP8.lab4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EquationTest {

    @Test
    public void calculationTest() {
        Equation equation = new Equation(new StringBuilder("(10 * (0.8 * -2.0) * 5.0 + (18.0 / 5.0) - 2)"));
        double result = equation.calculate();
        System.out.println(result);
        Assertions.assertEquals(result, 10 * (0.8 * -2.0) * 5.0 + (18.0 / 5.0) - 2, 0.1);
    }

    @Test
    public void syntaxTest() {
        Equation equation = new Equation(new StringBuilder("1.0 ** 25"));

        Assertions.assertThrows(IllegalArgumentException.class, equation::calculate);

        equation.setEquation(new StringBuilder("10 * ( * 0.8 * -2.0) * 5.0 + (18.0 / 5.0) - 2"));
        Assertions.assertThrows(IllegalArgumentException.class, equation::calculate);


        equation.setEquation(new StringBuilder("10 * (0.8 * -2.0() * 5.0 + ()18.0 / 5.0) - 2"));
        Assertions.assertThrows(IllegalArgumentException.class, equation::calculate);

    }

}
