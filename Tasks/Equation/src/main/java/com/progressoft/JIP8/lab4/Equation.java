package com.progressoft.JIP8.lab4;

import java.util.ArrayList;
import java.util.Stack;

public class Equation {
    private StringBuilder equation;
    private Stack<Double> operands;
    private Stack<String> operations;

    public Equation() {
        operands = new Stack();
        operations = new Stack();
    }

    public Equation(StringBuilder equation) {
        if (isNull(equation))
            throw new IllegalArgumentException("invalid Equation");
        this.equation = equation;
        operands = new Stack();
        operations = new Stack();
    }

    private boolean isNull(StringBuilder equation) {
        return equation == null || equation.equals("");
    }

    public void setEquation(StringBuilder equation) {
        if (isNull(equation))
            throw new IllegalArgumentException("invalid Equation");
        this.equation = equation;
    }

    public double calculate() {
        ArrayList<StringBuilder> parts = split();
        if (!isValid(parts)) {
            throw new IllegalArgumentException("invalid Equation Syntax");
        }

        try{
        return calculate(parts,0,parts.size());}
        catch (StringIndexOutOfBoundsException ignored) {}

        throw new IllegalArgumentException("invalid Equation");
    }

    private ArrayList<StringBuilder> split() {
        ArrayList<StringBuilder> parts = new ArrayList<StringBuilder>();
        for(int i =0, j =0 ; i<equation.length();i++,j++)
        {
            parts.add(new StringBuilder());
            while (i<equation.length() &&equation.charAt(i) != ' ')
            {
                parts.get(j).append(equation.charAt(i));
                i++;
            }
        }
        return parts;
    }

    private double calculate(ArrayList<StringBuilder> parts, int start, int end ) {
        for (int partindex = start; partindex < end; partindex++) {
            partindex = handlePart(parts, partindex);
        }

        while (!operations.isEmpty()) {
            calculateTopOperation();
        }

        return returnResult();
    }

    private int handlePart(ArrayList<StringBuilder> parts, int partIndex) {
        StringBuilder part = parts.get(partIndex);
        if (partIndex % 2 == 0) {
            if(part.indexOf("(") != -1) {

                int parenthesesClose = getParenthesesClose(parts,partIndex);
                double result = getResult(parts, partIndex, parenthesesClose);
                operands.push(result);
                return parenthesesClose;
            }
            else
            {operands.push(Double.parseDouble(String.valueOf(part)));
            return partIndex;}
        }

        return pushOperation(parts, partIndex, part);

    }

    private double getResult(ArrayList<StringBuilder> parts, int partIndex, int parenthesesClose) {
        parts.set(parenthesesClose,parts.get(parenthesesClose).deleteCharAt(parts.get(parenthesesClose).indexOf(")")));

        return new Equation().calculate(parts,partIndex,parenthesesClose+1);
    }

    private int getParenthesesClose(ArrayList<StringBuilder> parts, int partIndex) {
        Stack<String> parentheses = new Stack();
        parentheses.push("(");
        parts.get(partIndex).deleteCharAt(0);
        int current;
        for(current =partIndex+1;current<parts.size();current++)
        {
            if(parts.get(current).indexOf("(") != -1)
                parentheses.push("(");

            if(parts.get(current).indexOf(")") != -1)
            {
                if(parentheses.isEmpty())
                    break;
                parentheses.pop();
                if(parentheses.isEmpty())
                { return current;}
            }

        }

        if(current==parts.size())
            return current-1;

        throw new IllegalArgumentException("invalid equation");

    }

    private int pushOperation(ArrayList<StringBuilder> parts, int partIndex, StringBuilder part) {
        if ("/".equals(String.valueOf(part))) {
            double num1 = operands.pop();
            double num2;
            String nextInt = String.valueOf(parts.get(++partIndex));
            if(nextInt.startsWith("(")) {

                            int parenthesesClose = getParenthesesClose(parts,partIndex);
                            num2 = getResult(parts, partIndex, parenthesesClose);
                            operands.push(num1 /num2);
                            return parenthesesClose;
                        }
            else
            num2 = Double.parseDouble(nextInt);
            operands.push(num1 / num2);
        } else if ("*".equals(String.valueOf(part))) {
            double num1 = operands.pop();
            double num2;
            String nextInt = String.valueOf(parts.get(++partIndex));
            if(nextInt.startsWith("(")) {

                int parenthesesClose = getParenthesesClose(parts,partIndex);
                num2 = getResult(parts, partIndex, parenthesesClose);
                operands.push(num1 *num2);
                return parenthesesClose;
            }
            else
                num2 = Double.parseDouble(nextInt);
            operands.push(num1 * num2);
        } else {
            operations.push(String.valueOf(part));
        }
        return partIndex;
    }


    private double returnResult() {
        double result = operands.pop();
        operands.clear();
        operations.clear();

        return result;
    }


    private void calculateTopOperation() {
        String operation = operations.pop();
        Double num1 = operands.pop();
        Double num2 = operands.pop();
        if (operation.equals("+"))
            operands.push(num1 + num2);
        else if(operation.equals("-"))
            operands.push(num2 - num1);
    }


    private boolean isValid(ArrayList<StringBuilder> parts) {

        if (parts.size() < 3 && parts.size() % 2 != 1) {
            return false;
        }
        for (int partIndex = 0; partIndex < parts.size(); partIndex++) {
            StringBuilder part = parts.get(partIndex);

            int specify = partIndex % 2;

            if ( (specify == 0 && !isOperand(part) || (specify == 1 && !isOperation(part))))
                    return false;
    }
        return true;
    }

    private boolean isOperation(StringBuilder operation) {
        String operation_pattern = "([/*\\-+])";
        return String.valueOf(operation).matches(operation_pattern);
    }


    private boolean isOperand(StringBuilder operand) {
        String operand_pattern = "([(]*[+\\-]?\\d\\.?\\d?)+[)]*";
        return String.valueOf(operand).matches(operand_pattern);
    }
}
