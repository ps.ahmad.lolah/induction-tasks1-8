package com.progressoft.JIP8.lap7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class UseCase {
    public static void main(String[] args) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Employee employee = new Employee.EmployeeBuilder("Ahmad", "Lolah")
                .BirthDate(format.parse("2020-04-04"))
                .BirthPlace("Amman")
                .HiringDate(format.parse("2019-12-22"))
                .Position("Associate Java Developer")
                .ResignationDate(null)
                .Salary(450)
                .Build();

        Employee employee1 = new Employee.EmployeeBuilder("shady", "Lolah")
                .BirthDate(format.parse("2001-04-04"))
                .BirthPlace("AzZarqa")
                .HiringDate(format.parse("2018-12-22"))
                .Position("Associate Java Developer")
                .ResignationDate(null)
                .Salary(200)
                .Build();

        Employee employee2 = new Employee.EmployeeBuilder("Khalid", "Lolah")
                .BirthDate(format.parse("2001-04-04"))
                .BirthPlace("AzZarqa")
                .HiringDate(format.parse("2018-12-22"))
                .Position("Associate Java Developer")
                .ResignationDate(null)
                .Salary(200)
                .Build();

        Employee employee3 = new Employee.EmployeeBuilder("Ahmad", "Lolah")
                .BirthDate(format.parse("2020-04-04"))
                .BirthPlace("Amman")
                .HiringDate(format.parse("2019-12-22"))
                .Position("Associate Java Developer")
                .ResignationDate(null)
                .Salary(450)
                .Build();

        Employee employee4 = new Employee.EmployeeBuilder("shady", "Lolah")
                .BirthDate(format.parse("2001-04-04"))
                .BirthPlace("AzZarqa")
                .HiringDate(format.parse("2018-12-22"))
                .Position("Associate Java Developer")
                .ResignationDate(null)
                .Salary(200)
                .Build();

        EmployeeStatistics collector = new EmployeeStatistics();
        ArrayList<Employee> objects = new ArrayList<>();
        objects.add(employee);
        objects.add(employee1);
        objects.add(employee2);
        objects.add(employee3);
        objects.add(employee4);
        Iterable<Statistic> statistics = collector.collectStatistics(objects);
        System.out.printf("%-35s|%-45s%n", "Key", "Value");
        for (Statistic statistic : statistics) {
            System.out.println("*****************************************");
            System.out.printf("%-35s|%-45s%n", statistic.getKey(), statistic.getValue());
        }


    }
}