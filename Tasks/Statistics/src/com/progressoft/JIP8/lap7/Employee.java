package com.progressoft.JIP8.lap7;

import java.util.Date;

public class Employee {
    private final String FIRSTNAME;
    private final String LASTNAME;
    private final Date BIRTHDATE;
    private final String BIRTHPLACE;
    private final Date HIRINGDATE;
    private final Date RESIGNATIONDATE;
    private String position;
    private double salary;

    public Employee(EmployeeBuilder employeeBuilder) {
        this.FIRSTNAME = employeeBuilder.firstName;
        this.LASTNAME = employeeBuilder.lastName;
        this.BIRTHDATE = employeeBuilder.birthDate;
        this.BIRTHPLACE = employeeBuilder.birthPlace;
        this.HIRINGDATE = employeeBuilder.hiringDate;
        this.RESIGNATIONDATE = employeeBuilder.resignationDate;
        position = employeeBuilder.position;
        this.salary = employeeBuilder.salary;
    }


    public String getName() {
        return FIRSTNAME + " " + LASTNAME;
    }

    public Date getBIRTHDATE() {
        return BIRTHDATE;
    }

    public String getBIRTHPLACE() {
        return BIRTHPLACE;
    }

    public Date getHIRINGDATE() {
        return HIRINGDATE;
    }

    public Date getRESIGNATIONDATE() {
        return RESIGNATIONDATE;
    }

    public String getPosition() {

        return position;
    }

    public void setPosition(String position) {
        if (position.equals(""))
            throw new IllegalArgumentException("null Position");

        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        if (salary < 0)
            throw new IllegalArgumentException("negative salary");

        this.salary = salary;
    }

    public static class EmployeeBuilder {

        private String firstName;
        private String lastName;
        private Date birthDate;
        private String birthPlace;
        private Date hiringDate;
        private Date resignationDate;
        private String position;
        private double salary;

        public EmployeeBuilder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public EmployeeBuilder BirthDate(Date birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public EmployeeBuilder BirthPlace(String birthPlace) {
            this.birthPlace = birthPlace;
            return this;
        }

        public EmployeeBuilder HiringDate(Date hiringDate) {
            this.hiringDate = hiringDate;
            return this;
        }

        public EmployeeBuilder ResignationDate(Date resignationDate) {
            this.resignationDate = resignationDate;
            return this;
        }

        public EmployeeBuilder Position(String position) {
            this.position = position;
            return this;
        }

        public EmployeeBuilder Salary(double salary) {
            this.salary = salary;
            return this;
        }

        public Employee Build() {
            Employee employee = new Employee(this);
            validateEmployeeObject(employee);
            return employee;
        }

        private void validateEmployeeObject(Employee employee) {
            if (employee.FIRSTNAME.equals(""))
                throw new IllegalArgumentException("null First Name");

            if (employee.LASTNAME.equals(""))
                throw new IllegalArgumentException("null Last Name");

            if (employee.BIRTHDATE == null)
                throw new IllegalArgumentException("null BirthDate");

            if (employee.BIRTHPLACE.equals(""))
                throw new IllegalArgumentException("null Birth Place");

            if (employee.position.equals(""))
                throw new IllegalArgumentException("null Position");

            if (employee.HIRINGDATE ==null)
                throw new IllegalArgumentException("null Hiring Date");

            if (employee.salary < 0)
                throw new IllegalArgumentException("negative salary");

            if (employee.RESIGNATIONDATE != null &&employee.RESIGNATIONDATE.compareTo(employee.BIRTHDATE) <0)
                throw new IllegalArgumentException("");
        }
    }
}
