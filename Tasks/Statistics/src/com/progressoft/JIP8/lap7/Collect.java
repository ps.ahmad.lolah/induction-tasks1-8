package com.progressoft.JIP8.lap7;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Collect implements StatisticsCollector<String, Statistics> {

    public String name;

    public Collect() {
        this.name = "Collectors";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Iterable collectStatistics(Iterable<String> objects) {

        ArrayList<Statistics> statistics = new ArrayList<>();

        wordCount(objects, statistics);

        LowerCaseCollector(objects, statistics);

        DigitsCollector(objects, statistics);

        NonWordCollector(objects, statistics);

        upperCaseCollector(objects, statistics);

        return statistics;
    }

    private void upperCaseCollector(Iterable<String> objects, ArrayList<Statistics> statistics) {
        for (String str : objects) {

            int value = (int) str
                    .chars()
                    .filter(Character::isUpperCase)
                    .count();

            statistics.add(new Statistics(str, value));
        }
    }

    private void NonWordCollector(Iterable<String> objects, ArrayList<Statistics> statistics) {
        for (String str : objects) {

            int value = 0;
            Pattern pattern = Pattern.compile("[\\W ]");
            Matcher matcher = pattern.matcher(str.replaceAll(" ", ""));
            while (matcher.find()) {
                value++;
            }

            statistics.add(new Statistics("Non-Word Count", value));

        }
    }

    private void DigitsCollector(Iterable<String> objects, ArrayList<Statistics> statistics) {
        for (String str : objects) {

            int value = (int) str
                    .chars()
                    .filter(Character::isDigit)
                    .count();

            statistics.add(new Statistics("Digits Collector", value));

        }
    }

    private void LowerCaseCollector(Iterable<String> objects, ArrayList<Statistics> statistics) {
        for (String str : objects) {

            int value = (int) str
                    .chars()
                    .filter(Character::isLowerCase)
                    .count();

            statistics.add(new Statistics("Lower Case Collector", value));

        }
    }

    private void wordCount(Iterable<String> objects, ArrayList<Statistics> statistics) {
        for (String str : objects) {

            int value = str.split("\\s").length;

            statistics.add(new Statistics("Word Count", value));

        }
    }
}
