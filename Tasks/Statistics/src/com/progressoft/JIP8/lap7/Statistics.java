package com.progressoft.JIP8.lap7;

public class Statistics implements Statistic {

    private String key;
    private Object value;


    public Statistics(String key, Object value) {
        if (key == null)
            throw new IllegalArgumentException("Null Key");

        this.key = key;
        this.value = value;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
