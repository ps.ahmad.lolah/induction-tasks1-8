package com.progressoft.JIP8.lap7;

/**
 * Defines the implementer as a statistics collector strategy which accepts a
 * list of objects, collect required information (statistics) from them, then
 * returns a list of all calculated statistics.
 */
public interface StatisticsCollector<T extends Object, S extends Statistic> {
    /**
     * Returns a descriptive name of this collector, i.e: employees information
     * statistics
     *
     * @return collector name
     */
    String getName();

    /**
     * Accept then collect statistics from passed <code>objects</code>.
     * <p>
     *
     * @param object the objects to collect statistics from
     * @return calculated statistics
     */
    Iterable<S> collectStatistics(Iterable<T> objects);
}