package com.progressoft.JIP8.lap7;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EmployeeStatistics implements StatisticsCollector<Employee, Statistic> {

    ArrayList<Statistic> statistic;
    private String name;

    public EmployeeStatistics() {
        this.statistic = null;
        this.name = "Employee information Statistic ";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Iterable<Statistic> collectStatistics(Iterable<Employee> objects) {
        statistic = new ArrayList<>();

        salaryStatistics(objects);
        BirthPlaceStatistics(objects);
        BirthDateStatistics(objects);
        positionStatistics(objects);
        resignationStatistics(objects);

        return statistic;
    }

    private void resignationStatistics(Iterable<Employee> objects) {
        Map<Object, Integer> resignationStatistic = new HashMap<>();

        for (Employee employee : objects) {
            int value = 1;
            Date resignationYear= employee.getRESIGNATIONDATE();
            if(resignationYear == null)
                continue;
            if (resignationStatistic.containsKey(resignationYear))
                value += resignationStatistic.get(resignationYear);

            resignationStatistic.put(resignationYear, value);
        }

        addToStatistics(resignationStatistic, "resignation ");
    }

    public void positionStatistics(Iterable<Employee> objects) {
        Map<Object, Integer> positionStatistic = new HashMap<>();

        for (Employee employee : objects) {
            int value = 1;
            String position= employee.getPosition();
            if (positionStatistic.containsKey(position))
                value += positionStatistic.get(position);

            positionStatistic.put(position, value);
        }

        addToStatistics(positionStatistic, "Position ");
    }

    public void BirthDateStatistics(Iterable<Employee> objects) {
        Map<Object, Integer> birthDateStatistic = new HashMap<>();
        for (Employee employee : objects) {
            int value = 1;
            int birthyear = employee.getBIRTHDATE().getYear() - 100 + 2000;
            if (birthDateStatistic.containsKey(birthyear))
                value += birthDateStatistic.get(birthyear);

            birthDateStatistic.put(birthyear, value);
        }

        addToStatistics(birthDateStatistic, "Birth year ");
    }


    public void BirthPlaceStatistics(Iterable<Employee> objects) {
        Map<Object, Integer> birthPlaceStatistic = new HashMap<>();

        for (Employee employee : objects) {
            int value = 1;
            String birthplace= employee.getBIRTHPLACE();
            if (birthPlaceStatistic.containsKey(birthplace))
                value += birthPlaceStatistic.get(birthplace);

            birthPlaceStatistic.put(birthplace, value);
        }

        addToStatistics(birthPlaceStatistic, "Birth place ");

    }


    private void addToStatistics(Map<Object, Integer> birthDateStatic, String statisticType) {
        for (Map.Entry<Object, Integer> entry : birthDateStatic.entrySet()) {
            Object key = entry.getKey();
            Integer value = entry.getValue();
            statistic.add(new Statistics(statisticType + (key) , value));
        }
    }


    public void salaryStatistics(Iterable<Employee> objects) {
        int greatercount = 0;
        int lowercount = 0;
        for (Employee employee : objects) {
            if (employee.getSalary() >= 350 && employee.getSalary() < 600) {
                greatercount++;}
                if (employee.getSalary() < 350) {
                    lowercount++;
                      }
            }
            statistic.add(new Statistics("350 < Salary < 600", greatercount));
            statistic.add(new Statistics("Salary < 350", lowercount));
        }

    }