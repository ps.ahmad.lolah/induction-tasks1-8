package com.progressoft.JIP8.lap8;

import java.io.IOException;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) throws IOException {
        String path = "Stations.txt";

        StationsReader stationsReader = new StationsReader();

        HashMap<Integer, Stations> DataBase = stationsReader.read(path);

        StationsSearch stationsSearch = new StationsSearch(DataBase);
        stationsSearch.Search();

    }
}
