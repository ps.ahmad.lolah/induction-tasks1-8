package com.progressoft.JIP8.lap8;

public class Stations {
    private final String USAF ;
    private final String WBAN ;
    private String StationName;
    private final String CTRY ;
    private String ST ;
    private String CALL ;
    private String LAT ;
    private String LON ;
    private String ELEV ;
    private final String BEGIN ;
    private final String END ;

    public Stations(StationBuilder stationBuilder) {
        this.USAF = stationBuilder.USAF;
        this.WBAN = stationBuilder.WBAN;
        this.StationName = stationBuilder.STATIONNAME;
        this.CTRY  = stationBuilder.CTRY;
        this.ST = stationBuilder.ST;
        this.CALL = stationBuilder.CALL;
        this.LAT = stationBuilder.LAT;
        this.LON = stationBuilder.LON;
        this.ELEV = stationBuilder.ELEV;
        this.BEGIN = stationBuilder.BEGIN;
        this.END = stationBuilder.END;
    }

    public String getUSAF() {
        return USAF;
    }

    public String getWBAN() {
        return WBAN;
    }

    public String getStationName() {
        return StationName;
    }

    public String getCTRY() {
        return CTRY;
    }

    public String getST() {
        return ST;
    }

    public String getCALL() {
        return CALL;
    }

    public String getLAT() {
        return LAT;
    }

    public String getLON() {
        return LON;
    }

    public String getELEV() {
        return ELEV;
    }

    public String getBEGIN() {
        return BEGIN;
    }

    public String getEND() {
        return END;
    }

    @Override
    public String toString() {
        return  USAF
                +WBAN
                +StationName
                +CTRY
                +ST
                +CALL
                +LAT
                +LON
                +ELEV
                +BEGIN.substring(0,4)+"-"+BEGIN.substring(4,6)+"-"+BEGIN.substring(6,8)+" "
                +END.substring(0,4)+"-"+END.substring(4,6)+"-"+END.substring(6,8);
    }

    public static class StationBuilder{

        private  String USAF ;
        private  String WBAN ;
        private String STATIONNAME ;
        private  String CTRY ;
        private  String ST ;
        private  String CALL ;
        private  String LAT ;
        private  String LON ;
        private  String ELEV ;
        private String BEGIN ;
        private String END ;

        public StationBuilder USAF(String USAF) {
            this.USAF = USAF;
            return this;
        }

        public StationBuilder WBAN(String WBAN) {
            this.WBAN = WBAN;
            return this;
        }

        public StationBuilder STATIONNAME(String STATIONNAME) {
            this.STATIONNAME = STATIONNAME;
            return this;
        }

        public StationBuilder CTRY(String CTRY) {
            this.CTRY = CTRY;
            return this;
        }

        public StationBuilder ST(String ST) {
            this.ST = ST;
            return this;
        }

        public StationBuilder CALL(String CALL) {
            this.CALL = CALL;
            return this;
        }

        public StationBuilder LAT(String LAT) {
            this.LAT = LAT;
            return this;
        }

        public StationBuilder LON(String LON) {
            this.LON = LON;
            return this;
        }

        public StationBuilder ELEV(String ELEV) {
            this.ELEV = ELEV;
            return this;
        }

        public StationBuilder BEGIN(String BEGIN) {
            this.BEGIN = BEGIN;
            return this;
        }

        public StationBuilder END(String END) {
            this.END = END;
            return this;
        }

        public Stations Build()
        {
            Stations station = new Stations(this);
            ValidateStaionBuilder(station);
            return station;
        }

        private void ValidateStaionBuilder(Stations station) {

        }


    }

}
