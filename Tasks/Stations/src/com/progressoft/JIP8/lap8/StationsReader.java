package com.progressoft.JIP8.lap8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class StationsReader {
    private BufferedReader br;
    private HashMap<Integer, Stations> DataBase;


    public HashMap<Integer, Stations> read(String path) throws IOException {

        File file = new File(path);
        DataBase = new HashMap<>();
        br = new BufferedReader(new FileReader(file));

        HashMap<String, Column> template = getTemplate();

        read(template);

        return DataBase;
    }

    private void read(HashMap<String, Column> template) throws IOException {
        int Key = 0;
        String record;
        while ((record = br.readLine()) != null) {
            Stations station = new Stations.StationBuilder()
                    .USAF(record.substring(template.get("USAF").Start, template.get("USAF").End))
                    .WBAN(record.substring(template.get("WBAN").Start, template.get("WBAN").End))
                    .STATIONNAME(record.substring(template.get("STATIONNAME").Start, template.get("STATIONNAME").End))
                    .CTRY(record.substring(template.get("CTRY").Start, template.get("CTRY").End))
                    .ST(record.substring(template.get("ST").Start, template.get("ST").End))
                    .CALL(record.substring(template.get("CALL").Start, template.get("CALL").End))
                    .LAT(record.substring(template.get("LAT").Start, template.get("LAT").End))
                    .LON(record.substring(template.get("LON").Start, template.get("LON").End))
                    .ELEV(record.substring(template.get("ELEV(M)").Start, template.get("ELEV(M)").End))
                    .BEGIN((record.substring(template.get("BEGIN").Start, template.get("BEGIN").Start + 8)))
                    .END((record.substring(template.get("END").Start, template.get("END").Start + 8)))
                    .Build();

            DataBase.put(Key, station);
            Key++;
        }
    }

    private HashMap<String, Column> getTemplate() throws IOException {
        HashMap<String, Column> template = new HashMap<>();
        StringBuilder temp = new StringBuilder(br.readLine());
        int position = 0;
        for (int i = 0; i < temp.length(); i++) {

            while (i < temp.length() && temp.charAt(i) != ' ') {
                i++;

            }
            while (i < temp.length() && temp.charAt(i) == ' ') {
                i++;
            }
            template.put(temp.substring(position, i).trim(), new Column(position, i));
            position = i;
        }

        return template;
    }


    private class Column {
        public int Start;
        public int End;

        public Column(int start, int end) {
            Start = start;
            End = end;
        }
    }
}
