package com.progressoft.JIP8.lap8;

import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StationsSearch {
    public HashMap<Integer, Stations> DataBase;

    public StationsSearch(HashMap<Integer, Stations> DataBase) {
        this.DataBase = DataBase;
    }

    public void Search() {
        System.out.println("Enter your selection:\n" +
                "1 - Search for station by name\n" +
                "2- Search stations by country code\n" +
                "3- Search stations by stations ID range\n" +
                "4- Search stations by Geographical location.\n" +
                "5- Exit");
        Scanner scanner = new Scanner(System.in);
        int selection = scanner.nextInt();
        scanner.nextLine();
        switch (selection) {
            case 1:
                System.out.println("Enter Station Name:");
                String name = scanner.nextLine();
                SearchByName(name);
                break;
            case 2:
                System.out.println("Enter Country Code:");
                String countryCode = scanner.nextLine();
                SearchByCountryCode(countryCode);
                break;
            case 3:
                System.out.println("Enter StationID");
                String StationID = scanner.nextLine();
                SearchByStationID(StationID);
                break;
            case 4:
                System.out.println("Enter Geographical Location");
                String GEOLocation = scanner.nextLine();
                SearchByGeographicalLocation(GEOLocation);
                break;
            case 5:
                break;
            default:
                throw new IllegalArgumentException("Invalid Selection");
        }

    }

    private void SearchByGeographicalLocation(String GEOLocation) {
        printTemplate();


    }


    private void SearchByStationID(String stationID) {
        printTemplate();
        for (int i = 0; i < DataBase.size(); i++) {
            Stations station = DataBase.get(i);
            if (station.getUSAF().trim().equals(stationID.trim()))
                System.out.println(station.toString());
        }

    }

    private void SearchByCountryCode(String countryCode) {
        printTemplate();
        for (int i = 0; i < DataBase.size(); i++) {
            Stations station = DataBase.get(i);
            if (station.getCTRY().trim().equals(countryCode.trim()))
                System.out.println(station.toString());
        }
    }

    private void SearchByName(String name) {
        printTemplate();

        Pattern pattern = Pattern.compile(name);
        Matcher matcher = null;
        for (int i = 0; i < DataBase.size(); i++) {
            Stations station = DataBase.get(i);
            matcher = pattern.matcher(station.getStationName().trim());
            if (matcher.find())
                System.out.println(station.toString());
        }

    }

    private void printTemplate() {
        System.out.println("USAF   WBAN  STATIONNAME                   CTRY ST CALL  LAT     LON      ELEV(M) BEGIN      END");
    }

}
